// Insert a single room

db.Rooms.insert({
	Name: "Single",
	accomodation: "2",
	price: 1000,
	description: "A simple room with all the basic necessities",
	roomsAvailable: 10, 
	isAvailable: false
}); 

// Insert Many


db.Rooms.insertMany([
	{
		Name: "Double",
		accomodation: "3",
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		roomsAvailable: 5, 
		isAvailable: false
	},
	{
		Name: "Queen",
		accomodation: "4",
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		roomsAvailable: 15, 
		isAvailable: false
	}
]);


// Find Method

db.Rooms.find ({ 
name: "double"
});

// updateOne method

db.Rooms.updateOne(
	{ Name: "Queen"},
	{
		$set : {
			Name: "Queen",
			accomodation: "4",
			price: 4000,
			description: "A room with a queen sized bed perfect for a simple getaway",
			roomsAvailable: 0, 
			isAvailable: false
		}
	}
);

// deleteMany method

db.Rooms.deleteMany({
	roomsAvailable: 0
});

